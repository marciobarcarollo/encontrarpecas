<?php

/**
 * Description of Conexao
 * Classe para abstração de conecxao com o Banco de Dados Mysql. 
 *
 * @author marcio barcarrollo
 */
class Conexao {

    private static $cnx;

    public static function getConexao() {
        if (!self::$cnx) {
            Conexao::open();
        }
            return self:: $cnx;
    }

    /**
     * Variáveis no arquivo config/config.php.
     */
    private static function open() {

        $host = MYDB_HOST;
        $port = MYDB_PORT;
        $db_name = MYDB_NAME;
        $user_name = MYDB_USER_NAME;
        $password = MYDB_PASSWORD;
        try {
            self::$cnx = new PDO("mysql:host={$host};port={$port};dbname={$db_name}", "{$user_name}", "{$password}", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8"));
            self::$cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo " Erro " . $e->getMessage() . "<br/>";
            
            // TRATAR ERRO.
            
        }
        
    }

}

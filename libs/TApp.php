<?php

/**
 * Description of Tapp
 *
 * @author marcio barcarrollo
 */
class TApp {
    #Atributos

    private $to;
    private $method;
    private $params;

    #Métodos

    public function __construct() {

        $url = isset($_GET['url']) ? $_GET['url'] : FALSE;
        $url = rtrim($url, "/");

        //http://localhost/New_Webpecas/contas-a-pagar/pagamentos
        if ($url) {

            $arr = strtolower($url);
            $arr = explode('/', $url);

            //TRATAR O TO URL
            if (isset($arr[0])) {
                $to = explode("-", $arr[0]);
                $strTo = "";
                foreach ($to as $k => $v) {
                    $strTo .= strtoupper(substr($v, 0, 1)) . substr($v, 1);
                }
                $this->to = $strTo;
            }

            //TRATAR O METHOD URL
            if (isset($arr[1])) {
                $mt = explode("-", $arr[1]);
                $strMt = "";
                foreach ($mt as $k => $v) {
                    if ($k === 0) {
                        $strMt .= $v;
                    } else {
                        $strMt .= strtoupper(substr($v, 0, 1)) . substr($v, 1);
                    }
                }
                $this->method = $strMt;
            }

            //TRATAR PARAMS de URL
            unset($arr[0]);
            unset($arr[1]);
            $this->params = $arr;
        } else {
            $this->to = "home";
            $this->method = "principal";
            $this->params = NULL;
        }
    }

    public function executar() {
        if (class_exists($this->to)) {
            try {
                $c = new $this->to();
                if ($c instanceof IPrivateTo) {
                    if (!$_SESSION['usuario']['logado']) {
                        header("location:" . URL ."login/autenticar");
                    }
                }
                    if (method_exists($c, $this->method)) {
                        if ($this->params !== NULL) {
                            $c->{$this->method}($this->params);
                        } else {
                            $c->{$this->method}();
                        }
                    } else {
                        // tratar erro
                    }
                
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        } else {
            //Tratar Erros
        }
    }

}

?>
<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Email
 *
 * @author marcio barcarrollo
 */
class Email {
    #Atributos

    private $id;
    private $email;
    private $verificado;
    private $principal;

#Metodos

    function getId() {
        return $this->id;
    }

    function getEmail() {
        return $this->email;
    }

    function getVerificado() {
        return $this->verificado;
    }

    function getPrincipal() {
        return $this->principal;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setVerificado($verificado) {
        $this->verificado = $verificado;
    }

    function setPrincipal($principal) {
        $this->principal = $principal;
    }

}

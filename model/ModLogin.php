<?php

/**
 * Description of Login
 *
 * @author marcio barcarrollo
 */
class ModLogin {
    # Atributos  // id, data, idSession, idUsuario

    private $id;
    private $data;
    private $idSession;
    private $idUsuario;
    #Atributos auxiliares 
    private $login;
    private $erro;
    private $sucesso;
#Métodos
    
    function getId() {
        return $this->id;
    }

    function getData() {
        return $this->data;
    }

    function getIdSession() {
        return $this->idSession;
    }

    function getIdUsuario() {
        return $this->idUsuario;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setIdSession($idSession) {
        $this->idSession = $idSession;
    }

    function setIdUsuario($idUsuario) {
        $this->idUsuario = $idUsuario;
    }
    function getErro() {
        return $this->erro;
    }

    function setErro($erro) {
        $this->erro = $erro;
    }
    function getSucesso() {
        return $this->sucesso;
    }

    function setSucesso($sucesso) {
        $this->sucesso = $sucesso;
    }
    function getLogin() {
        return $this->login;
    }

    function setLogin($login) {
        $this->login = $login;
    }


    
}

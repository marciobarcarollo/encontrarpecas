<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Session
 *
 * @author marcio barcarrollo
 */
class Session {
#Atributos  

    private $idSession;
    private $tokemSession;
    private $httpReferer;
    private $dataSession;
    private $ip;
    private $erro;
    private $sucesso;

#Metodos    

    function getIdSession() {
        return $this->idSession;
    }

    function getTokemSession() {
        return $this->tokemSession;
    }

    function getHttpReferer() {
        return $this->httpReferer;
    }

    function getDataSession() {
        return $this->dataSession;
    }

    function getIp() {
        return $this->ip;
    }

    function setIdSession($idSession) {
        $this->idSession = $idSession;
    }

    function setTokemSession($tokemSession) {
        $this->tokemSession = $tokemSession;
    }

    function setHttpReferer($httpReferer) {
        $this->httpReferer = $httpReferer;
    }

    function setDataSession($dataSession) {
        $this->dataSession = $dataSession;
    }

    function setIp($ip) {
        $this->ip = $ip;
    }
    function getErro() {
        return $this->erro;
    }

    function getSucesso() {
        return $this->sucesso;
    }

    function setErro($erro) {
        $this->erro = $erro;
    }

    function setSucesso($sucesso) {
        $this->sucesso = $sucesso;
    }


}

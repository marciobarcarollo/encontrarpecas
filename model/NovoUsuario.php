<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NovoUsuario
 *
 * @author marcio barcarrollo
 */
class NovoUsuario extends Usuario {

    private $email;
    private $emailConfirmacao;
    private $senhaConfirmacao;
    private $erro;
    private $sucesso;

    function getEmail() {
        return $this->email;
    }

    function getEmailConfirmacao() {
        return $this->emailConfirmacao;
    }

    function getSenhaConfirmacao() {
        return $this->senhaConfirmacao;
    }

    function getErro() {
        return $this->erro;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setEmailConfirmacao($emailConfirmacao) {
        $this->emailConfirmacao = $emailConfirmacao;
    }

    function setSenhaConfirmacao($senhaConfirmacao) {
        $this->senhaConfirmacao = $senhaConfirmacao;
    }

    function setErro($erro) {
        $this->erro = $erro;
    }

    function getSucesso() {
        return $this->sucesso;
    }

    function setSucesso($sucesso) {
        $this->sucesso = $sucesso;
    }

}

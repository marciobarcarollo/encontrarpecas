<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EmailHasUsuario
 *
 * @author marcio barcarrollo
 */
class EmailHasUsuario {

    private $usuarioId;
    private $emailId;
    private $erro;
    private $sucesso;

    function getUsuarioId() {
        return $this->usuarioId;
    }

    function getEmailId() {
        return $this->emailId;
    }

    function setUsuarioId($usuarioId) {
        $this->usuarioId = $usuarioId;
    }

    function setEmailId($emailId) {
        $this->emailId = $emailId;
    }
    function getErro() {
        return $this->erro;
    }

    function getSucesso() {
        return $this->sucesso;
    }

    function setErro($erro) {
        $this->erro = $erro;
    }

    function setSucesso($sucesso) {
        $this->sucesso = $sucesso;
    }



}

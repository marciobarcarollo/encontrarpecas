<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Geoip
 *
 * @author marcio barcarrollo
 */
class Geoip {
    #Atributos
    private $ip;
    private $countryCode;
    private $regionCode;
    private $cityName;
    private $postalCode;
    private $latitude;
    private $longitude;
    private $metroCode;
    private $areaCode;
    private $ispName;
    private $organizationName;
    private $error;
    private $data;
    private $status;

#Metodos
    function getIp() {
        return $this->ip;
    }

    function getCountryCode() {
        return $this->countryCode;
    }

    function getRegionCode() {
        return $this->regionCode;
    }

    function getCityName() {
        return $this->cityName;
    }

    function getPostalCode() {
        return $this->postalCode;
    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude() {
        return $this->longitude;
    }

    function getMetroCode() {
        return $this->metroCode;
    }

    function getAreaCode() {
        return $this->areaCode;
    }

    function getIspName() {
        return $this->ispName;
    }

    function getOrganizationName() {
        return $this->organizationName;
    }

    function getError() {
        return $this->error;
    }

    function getData() {
        return $this->data;
    }

    function getStatus() {
        return $this->status;
    }

    function setIp($ip) {
        $this->ip = $ip;
    }

    function setCountryCode($countryCode) {
        $this->countryCode = $countryCode;
    }

    function setRegionCode($regionCode) {
        $this->regionCode = $regionCode;
    }

    function setCityName($cityName) {
        $this->cityName = $cityName;
    }

    function setPostalCode($postalCode) {
        $this->postalCode = $postalCode;
    }

    function setLatitude($latitude) {
        $this->latitude = $latitude;
    }

    function setLongitude($longitude) {
        $this->longitude = $longitude;
    }

    function setMetroCode($metroCode) {
        $this->metroCode = $metroCode;
    }

    function setAreaCode($areaCode) {
        $this->areaCode = $areaCode;
    }

    function setIspName($ispName) {
        $this->ispName = $ispName;
    }

    function setOrganizationName($organizationName) {
        $this->organizationName = $organizationName;
    }

    function setError($error) {
        $this->error = $error;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setStatus($status) {
        $this->status = $status;
    }


}

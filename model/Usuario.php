<?php

/**
 * Description of Usuario
 *
 * @author marcio barcarrollo
 */
class Usuario {
#Atributos
    /**
     *
     * @var int 
     */

    private $id;

    /**
     *
     * @var $string varchar(45)
     */
    private $nome;

    /**
     *
     * @var $string varchar(45)
     */
    private $sobreNome;

    /**
     *
     * @var $string varchar(45)
     */
    private $verificado;

    /**
     *
     * @var $string varcahr(32)
     */
    private $senha;

    /**
     *
     * @var $string char(1)
     */
    private $status;
    private $dataCadastro;

#Metodos    

    public function __construct() {
        $this->id;
    }

    /**
     * 
     * @return int Usuario
     */
    function getId() {
        return $this->id;
    }

    /**
     * 
     * @return Usuario
     */
    function getNome() {
        return $this->nome;
    }

    function getStatus() {
        return $this->status;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function getSenha() {
        return $this->senha;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function getSobreNome() {
        return $this->sobreNome;
    }

    function setSobreNome($sobreNome) {
        $this->sobreNome = $sobreNome;
    }

    function getVerificado() {
        return $this->verificado;
    }

    function setVerificado($verificado) {
        $this->verificado = $verificado;
    }

    function getDataCadastro() {
        return $this->dataCadastro;
    }

    function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

}

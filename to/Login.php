<?php

/**
 * Description of Login
 *
 * @author marcio barcarrollo
 */
class Login {

    public function autenticar() {
        $v = new TGui("formularioDeLogin");
        $v->renderizar();
    }

    public function confirmarAutenticacao() {
        $erro = array();
        $l = new ModLogin();

        $login = isset($_POST['login']) ? $_POST['login'] : FALSE;
        if (!$login || trim($login) === "") {
            $erro[] = "O campo Login é Obrigatorio";
        }

        $senha = isset($_POST['senha']) ? $_POST['senha'] : FALSE;
        if (!$senha || trim($senha) === "") {
            $erro[] = "O campo Senha é Obrigatorio";
        }
        if ($login || $senha) {
            $dem = new DaoEmail();
            $lista = $dem->listar($login);
            if (!$lista) {
                $erro[] = "Email não Cadastrado no Sistema";
            }
        }
        if ($erro) {
            $l->setErro($erro);
            $l->setLogin($login);
            $this->erroLogin($l);
        } else {

            $du = new DaoUsuario();
            $ret = $du->autenticar($login, md5($senha));
            if ($ret) {
                if ($ret['usuarioVerificado'] != 1) {
                    unset($_SESSION['usuario']);
                    $erro[] = "É necessário realizar a verificação de Usuário para o primeiro acesso.";
                    $erro[] = "Consulte seu Email e realize a verificação";
                    $l->setErro($erro);
                    $l->setLogin($login);
                    $this->erroLogin($l);
                } else {
                    if ($ret['emailVerificado'] != 1) {
                        unset($_SESSION['usuario']);
                        $erro[] = "Email de Usuário não Verificado Acesse com email verificado.";
                        $erro[] = "Consulte seu Email e realize a verificação";
                        $l->setErro($erro);
                        $l->setLogin($login);
                        $this->erroLogin($l);
                    } else {
                        foreach ($ret as $key => $value) {
                            $_SESSION['usuario'][$key] = $value;
                        }
                        $_SESSION['usuario']['logado'] = '1';

                        // REGISTRAMOS O LOGIN
                        $id = "";
                        $data = date("Y-m-d H:i:s");
                        $idSession = $_SESSION['session']['id'];
                        $idUsuario = $_SESSION['usuario']['id'];

                        $l = new ModLogin();

                        $l->setId($id);
                        $l->setData($data);
                        $l->setIdSession($idSession);
                        $l->setIdUsuario($idUsuario);

                        $dl = new DaoLogin();
                        $salvar_dl = $dl->salvar($l);

                        if ($salvar_dl) {
                            $_SESSION['usuario']['login']['id'] = $salvar_dl->getId();
                        } else {
                            unset($_SESSION['usuario']);
                            $erro[] = "Não foi possível o Cadastro de Login ";
                            $l->setErro($erro);
                            $l->setLogin($login);
                            $this->erroLogin($l);
                        }
                        header("location:" . URL);
                    }
                }
            } else {
                unset($_SESSION['usuario']);

                $erro[] = "Senha inválida";
                $l->setErro($erro);
                $l->setLogin($login);
                $this->erroLogin($l);
            }
        }
    }

    public function logout() {
        unset($_SESSION['usuario']);
        unset($_SESSION['garagem']);
        header("location:" . URL);
    }

    public function cadastrar() {
        $v = new TGui("formularioCadastroUsuario");
        $v->renderizar();
    }

    public function recuperarLogin() {
        $v = new TGui("formularioRecuperaSenha");
        $v->renderizar();
    }

    public function confirmarRecuperacaoLogin() {
        $erro = array();
        $sucesso = array();
        $l = new ModLogin();

        $login = isset($_POST['login']) ? $_POST['login'] : FALSE;
        if (!$login || trim($login) === "") {
            $erro[] = "O campo Login é Obrigatorio";
            $l->setErro($erro);
            $this->erroRecuperarSenha($l);
        }
        if ($login) {
            $dem = new DaoEmail();
            $lista = $dem->listar($login);
            if (!$lista) {
                $erro[] = "Email não Cadastrado no Sistema";
                $l->setErro($erro);
                $this->erroRecuperarSenha($l);
            } else {
                $lista instanceof Email;
                if ($lista->getVerificado() != 0) {

                    $sucesso[] = "Enviado email para redefinição de Senha";
                    $l->setSucesso($sucesso);
                    $this->erroRecuperarSenha($l);
                } else {

                    $erro[] = "Email não verificado";
                    $sucesso[] = "Enviado email para verificação.";
                    $l->setErro($erro);
                    $l->setSucesso($sucesso);
                    $this->erroRecuperarSenha($l);
                }
            }
        }
    }

    public function verificarUsuario($p1) {
        $erro = array();
        $sucesso = array();

        $l = new ModLogin;

        $dados = isset($p1['2']) ? $p1['2'] : FALSE;
        if ($dados[0] && $dados[1]) {

            $dados = base64_decode($dados);
            $dados = explode('-', $dados);

            $emailId = isset($dados[0]) ? $dados[0] : FALSE;
            if (!$emailId) {
                $erro[] = 'Email inválido';
            }
            $usuarioId = isset($dados[1]) ? $dados[1] : FALSE;
            if (!$usuarioId) {
                $erro[] = 'Usuário inválido';
            }
            if (!$erro == "") {
                $l->setErro($erro);
                $l->setSucesso($sucesso);
                $this->erroLogin($l);
            } else {

                $ehu = new EmailHasUsuario();
                $ehu->setEmailId($emailId);
                $ehu->setUsuarioId($usuarioId);

                $arr = array();
                $arr['usuarioId'] = $ehu->getUsuarioId();
                $arr['emailId'] = $ehu->getEmailId();

                $dehu = new DaoEmailHasUsuario();
                $lista = $dehu->listar($arr);

                if ($lista) {

                    $usuarioId = $lista['usuarioId'];

                    $u = new Usuario();
                    $u->setStatus('1');
                    $u->setVerificado('1');
                    $u->setId($usuarioId);

                    $du = new DaoUsuario();
                    $ver_usuario = $du->verificar($u);

                    if ($ver_usuario) {
                        $sucesso[0] = "Usuario e ";
                    } else {
                        $erro[] = "Não Foi possível Verificar Usuário";
                    }

                    $emailId = $lista['emailId'];

                    $e = new Email();
                    $e->setId($emailId);
                    $e->setVerificado('1');

                    $de = new DaoEmail();
                    $ver_email = $de->verificar($e);

                    if ($ver_email) {
                        $sucesso[0] .= " Email";
                    } else {
                        $erro[] = "Não Foi possível Verificar Email";
                    }
                    if (!$erro == "") {
                        $l->setErro($erro);
                        $l->setSucesso($sucesso);
                        $this->erroLogin($l);
                    } else {

                        $sucesso[0] .= ' Verificados com sucesso';

                        $l->setErro($erro);
                        $l->setSucesso($sucesso);
                        $this->erroLogin($l);
                    }
                } else {
                    $erro[] = 'Email ou Usuário inválidos';
                    $l->setErro($erro);
                    $l->setSucesso($sucesso);
                    $this->erroLogin($l);
                }
            }
        } else {
            $erro[] = 'Parametros inválidos';
            $l->setErro($erro);
            $l->setSucesso($sucesso);
            $this->erroLogin($l);
        }
    }

    public function erroLogin(ModLogin $l) {
        $v = new TGui("formularioDeLogin");
        $v->addDados('modLogin', $l);
        $v->renderizar();
    }

    public function erroRecuperarSenha(ModLogin $l) {
        $v = new TGui("formularioRecuperaSenha");
        $v->addDados('modLogin', $l);
        $v->renderizar();
    }

}

<?php

/**
 * Description of ControleUsuario
 *
 * @author marcio barcarrollo
 */
class ControleUsuario {

    public function listaDeUsuario() {
        $du = new DaoUsuario();
        $usuarios = $du->listarTodos();
        $v = new TGui("listaDeUsuario");
        $v->addDados('usuarios', $usuarios);
        $v->renderizar();
    }

    public function editar($id) {
        $p1 = $id[2];
        $du = new DaoUsuario();
        $usuario = $du->listar($p1);
        $v = new TGui("formularioUsuario");
        $v->addDados('usuario', $usuario);
        $v->renderizar();
    }

    public function novo() {
        $v = new TGui("formularioUsuario");
        $v->renderizar();
    }

    public function salvar() {
        GLOBAL $webpecas;
        
        $erro = array();
        $sucesso = array();
        $verificacao = ""; // Geramos o get de verificacao;
        $nu = new NovoUsuario();

        $nome = isset($_POST['nome']) ? $_POST['nome'] : FALSE;
        if (!$nome || trim($nome) === "") {
            $nu->setNome($nome);
            $erro[] = "O campo nome é Obrigatorio";
        } else {
            $nome = strtoupper(substr($nome, 0, 1)) . substr($nome, 1);
        }
        $sobreNome = isset($_POST['sobreNome']) ? $_POST['sobreNome'] : FALSE;
        if (!$sobreNome || trim($sobreNome) === "") {
            $nu->setSobreNome($sobreNome);
            $erro[] = "O campo Sobre nome é Obrigatorio";
        } else {
            $sobreNome = strtoupper(substr($sobreNome, 0, 1)) . substr($sobreNome, 1);
        }
        $email = isset($_POST['email']) ? $_POST['email'] : FALSE;
        if (!$email || trim($email) === "") {
            $nu->setEmail($email);
            $erro[] = "O campo Email é Obrigatorio";
        } else {
            $email = strtolower($email);
        }
        $emailConfirmacao = isset($_POST['emailConfirmacao']) ? $_POST['emailConfirmacao'] : FALSE;
        if (!$emailConfirmacao || trim($emailConfirmacao) === "") {
            $nu->setEmail($emailConfirmacao);
            $erro[] = "O campo Email Confirmação é Obrigatorio";
        }
        $senha = isset($_POST['senha']) ? $_POST['senha'] : FALSE;
        if (!$senha || trim($senha) === "") {
            $nu->setSenha($senha);
            $erro[] = "O campo Senha é Obrigatorio";
        }
        $senhaConfirmacao = isset($_POST['senhaConfirmacao']) ? $_POST['senhaConfirmacao'] : FALSE;
        if (!$senhaConfirmacao || trim($senhaConfirmacao) === "") {
            $nu->setSenhaConfirmacao($senhaConfirmacao);
            $erro[] = "O campo Senha de Confirmação é Obrigatorio";
        }
        $status = isset($_POST['status']) ? $_POST['status'] : FALSE;
//        if (!$status || trim($status) === "") {
//            $nu->setStatus($status);
//            $erro[] = "O campo Status é Obrigatorio";
//        }
        $verificado = isset($_POST['verificado']) ? $_POST['verificado'] : FALSE;
//        if (!$verificado || trim($verificado) === "") {
//            $nu->setVerificado($verificado);
//            $erro[] = "O campo Verificado é Obrigatorio";
//        }
        if ($senha AND $senhaConfirmacao) {
            if ($senha != $senhaConfirmacao) {
                $erro[] = "O campo Senha e Confirmação de Senha devem ser iguais.";
            }
        }
        if ($email AND $emailConfirmacao) {
            if ($email != $emailConfirmacao) {
                $erro[] = "O campo Email e Confirmação de Email devem ser iguais.";
            } else {
                $dem = new DaoEmail();
                $lista = $dem->listar($email);
                if ($lista instanceof Email) {
                    $erro[] = "Email já Cadastrado no Sistema Recupere seu email Acessando <a href=\"".URL."login/recuperarLogin\">Recuperar Senha</a>";
                }
            }
        }
        $dataCadastro = isset($_POST['dataCadastro']) ? $_POST['dataCadastro'] : FALSE;
        if (!$dataCadastro || trim($dataCadastro) === "") {
            $dataCadastro = date("Y-m-d H:i:s");
        }

        $nu->setNome($nome);
        $nu->setSobreNome($sobreNome);
        $nu->setSenha($senha);
        $nu->setSenhaConfirmacao($senhaConfirmacao);
        $nu->setEmail($email);
        $nu->setEmailConfirmacao($emailConfirmacao);
        $nu->setStatus($status);
        $nu->setVerificado($verificado);
        $nu->setDataCadastro($dataCadastro);
        $nu->setErro($erro);

        if (!$erro == "") {
            $v = new TGui("formularioCadastroUsuario");
            $v->addDados('usuario', $nu);
            $v->renderizar();
        } else {

            //CADASTRAMOS O USUARIO.
            $u = new Usuario();
            $u->setId($nu->getId());
            $u->setNome($nu->getNome());
            $u->setSobreNome($nu->getSobreNome());
            $u->setSenha(md5($nu->getSenha()));
            $u->setDataCadastro($nu->getDataCadastro());
            $u->setStatus($nu->getStatus());
            $u->setVerificado($nu->getVerificado());

            $du = new DaoUsuario();
            $usu = $du->salvar($u);

            if ($usu instanceof Usuario) {
                $verificacao .= $nu->getId() . "/";

                $e = new Email();
                $e->setEmail($nu->getEmail());
                $e->setPrincipal('1');
                $e->setVerificado('0');

                $de = new DaoEmail();
                $em = $de->salvar($e);
                if ($em instanceof Email) {

                    $ehu = new EmailHasUsuario();
                    $ehu->setEmailId($e->getId());
                    $ehu->setUsuarioId($u->getId());

                    $dehu = new DaoEmailHasUsuario();
                    $res_ehu = $dehu->salvar($ehu);

                    if ($res_ehu instanceof EmailHasUsuario) {

                        $verificacao .= base64_encode($ehu->getEmailId() . "-" . $ehu->getUsuarioId());
                        $urlVerificacao = URL . "login/verificar-usuario" . $verificacao;

                        // ENVIAR EMAIL PARA VERIFICACAO
                        require_once './email/emailConfirmacaoDeUsuario.php';
                        
                        $sendEmail = mail($to, $subject, $message, $headers);

                        if ($sendEmail) {
                            $sucesso[] = 'Cadastro efetuado com sucesso';
                            $nu->setErro("");
                        } else {
                            $erro[] = 'Falha ao enviar o email';
                            $nu->setSucesso($sucesso);
                        }

                        $nu->setNome("");
                        $nu->setSobreNome("");
                        $nu->setSenha("");
                        $nu->setSenhaConfirmacao("");
                        $nu->setEmail("");
                        $nu->setEmailConfirmacao("");
                        $nu->setStatus("");
                        $nu->setVerificado("");
                        $nu->setDataCadastro("");


                        $nu->setSucesso($sucesso);
                        $nu->setErro($erro);

                        $v = new TGui("formularioCadastroUsuario");
                        $v->addDados('usuario', $nu);
                        $v->renderizar();
                    } else {
                        $erro[] = 'Não foi possivel cadastrar o relacionamento de email';
                        $nu->setErro($erro);
                        $v = new TGui("formularioCadastroUsuario");
                        $v->addDados('usuario', $nu);
                        $v->renderizar();
                    }
                    return;
                } else {
                    $erro[] = 'Não foi possivel cadastrar o email para o usuario';
                    $nu->setErro($erro);
                    $v = new TGui("formularioCadastroUsuario");
                    $v->addDados('usuario', $nu);
                    $v->renderizar();
                }
            } else {
                $erro[] = 'Não foi possivel cadastrar o usuario';
                $nu->setErro($erro);
                $v = new TGui("formularioCadastroUsuario");
                $v->addDados('usuario', $nu);
                $v->renderizar();
            }
        }
    }

    public function excluir($id) {
        $p1 = $id[2];
        $du = new DaoUsuario();
        $usuario = $du->listar($p1);
        $v = new TGui("confirmaExclusaoUsuario");
        $v->addDados('usuario', $usuario);
        $v->renderizar();
    }

    public function confirmaExclusao() {
        $id = isset($_POST['id']) ? $_POST['id'] : FALSE;
        if ($id) {
            $du = new DaoUsuario();
            $u = $du->listar($id);
            if ($du->excluir($u)) {
                header("location:" . URL . "controle-usuario/lista-de-usuario");
            } else {
                echo "Não foi possível excluir";
            }
        } else {
            header("location:" . URL . "controle-usuario/lista-de-usuario");
        }
    }

}

<?php

// Include MapBuilder class.
require_once 'class.MapBuilder.php';

// Create MapBuilder object.
$map = new MapBuilder();

// Set map's center position by latitude and longitude coordinates. 
$map->setCenter($_SESSION['geoip']['latitude'], $_SESSION['geoip']['longitude']);

// Add a marker with specified color and symbol. 
$map->addMarker($_SESSION['geoip']['latitude'], $_SESSION['geoip']['longitude'], array(
    'title' => 'Eu no Mapa',
    'defColor' => '#1717DE',
    'defSymbol' => '',
    'icon' => 'http://labs.google.com/ridefinder/images/mm_20_blue.png',
    'shadow' => 'http://labs.google.com/ridefinder/images/mm_20_shadow.png',
    'html' => '<b>nome da empresa</b> <br/> Endereço <br/>Site:<a href="http://www.webpecas.com.br">www.webpecas.com.br</a>'
));
$points = new DaoMarker();
$lista = $points->listar();

foreach ($lista as $marker ) {
    $marker instanceof Marker;
    // Add a marker with specified color and symbol. 
    $map->addMarker($marker->getLat(), $marker->getLng(), array(
        'title' => $marker->getName(),
        'defColor' => '#1717DE',
        'defSymbol' => '',
        'icon' => 'imagem/wb25x25.png',
        'shadow' => 'http://labs.google.com/ridefinder/images/mm_20_shadow.png',
        'html' => '<b>' . $marker->getName() . '</b> <br/>' . $marker->getAddress() . '<br/>Telefone:55 11 43384777<br/>Site:<a href="http://www.webpecas.com.br">www.webpecas.com.br</a>'
    ));
}

// Set the default map type.
$map->setMapTypeId(MapBuilder::MAP_TYPE_ID_ROADMAP);

// Set width and height of the map.
$map->setFullScreen(false);
$map->setSize(850 , 450);
        
// Set default zoom level.
$map->setZoom(12);

// Set minimum and maximum zoom levels.
$map->setZoomControl(true);
$map->setMinZoom(3);
$map->setMaxZoom(19);

// Set Street View ( false, true).
$map->setStreetViewControl(true);

// Disable mouse scroll wheel.
$map->setScrollwheel(true);

// Make the map draggable.
$map->setDraggable(true);

// Enable zooming by double click.
$map->setDisableDoubleClickZoom(false);

// Disable all on-map controls.
$map->setDisableDefaultUI(true);

// Display the map.
$map->show();

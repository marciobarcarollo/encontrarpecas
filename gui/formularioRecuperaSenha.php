<?php
$erro = "";
$sucesso = "";

$l = $this->getDados("modLogin");

if ($l) {

    $l instanceof ModLogin;

    $erro = $l->getErro();
    $sucesso = $l->getSucesso();
}
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Recuperação de Senha para Usuário</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo URL;?>inc/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo URL;?>inc/bootstrap/css/signin.css" rel="stylesheet">
    
  </head>

  <body>

    <div class="container">
        <form class="form-signin"  method="POST" action="<?php echo URL ;?>login/confirmar-recuperacao-login">
         <?php
                    IF ($erro) {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">";
                        foreach ($erro as $value) {
                            echo "<p><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>";
                            echo "<span class=\"sr-only\">Error:</span>";
                            echo $value . "</p>";
                        }
                        echo "</div>";
                    }
                    if ($sucesso) {
                        //echo "<div class=\"alert alert-sucsses\" role=\"alert\">";
                        foreach ($sucesso as $value) {
                            echo "<p class=\"bg-success\"><i class=\"glyphicon glyphicon-user\"></i> - {$value} </p>";
                        }
                        //echo "</div>";
                    }
                    ?>      
        <a href="<?php echo URL ;?>"><img src="<?php echo URL ;?>imagem/logoWebpecas.png" class="img-responsive" alt="Responsive image"></a>
        <label class="sr-only">Recuperar Senhaou Login</label>
        <label for="inputEmail" class="sr-only">Login ou Email</label>
        <input type="text" id="inputEmail" class="form-control" name="login" placeholder="Email" > <!-- required autofocus -->
        <div>
            <br>
            
        </div>
        <button class="btn btn-lg btn-success btn-block" type="submit">Entrar</button>
        <hr> </hr>
        <a class="btn btn-lg btn-default btn-block" href="<?php echo URL ;?>login/cadastrar" role="button">Cadastre-se</a>
      </form>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo URL ;?>inc/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>




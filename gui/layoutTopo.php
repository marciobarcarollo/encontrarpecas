<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo URL ;?>imagem/favicon.ico">

        <title>Webpecas Cotações Online de Peças</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo URL; ?>inc/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo URL; ?>inc/bootstrap/css/jumbotron.css" rel="stylesheet">

    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo URL; ?>"><img src="<?php echo URL; ?>imagem/logoWebpecasNavBar.png" alt="Site Webpeças"></a>
                </div>

                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo URL; ?>para-voce/como-funciona">Para Você</a></li>
                        <li><a href="<?php echo URL; ?>para-sua-empresa/como-funciona">Para sua Empresa</a></li>
                        <li><a href="<?php echo URL; ?>contato">Contato</a></li>
                    </ul>  
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                        if (isset($_SESSION['usuario']['logado'])) {
                            if ($_SESSION['usuario']['logado'] == TRUE) {
                                include_once './gui/menuHomeUsuarioLogado.php';
                            } else {
                                include './gui/menuHomeUsuarioLogout.php';
                            }
                        } else {
                            include './gui/menuHomeUsuarioLogout.php';
                        }
                        ?>
                    </ul>
                </div><!--/.navbar-collapse -->
            </div><!-- /container -->
        </nav>


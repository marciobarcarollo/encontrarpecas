<?php require"layoutTopo.php"; ?>

<div class="container-fluid">
    <?php
    if (isset($_POST['BTEnvia'])) {

        //Variaveis de POST, Alterar somente se necessário 
        //====================================================
        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $mensagem = $_POST['mensagem'];
        global $webpecas;
        //====================================================
        //REMETENTE --> ESTE EMAIL TEM QUE SER VALIDO DO DOMINIO
        //====================================================
        switch ($_POST['destinatario']) {
            case '0' : $destinatario = $webpecas['email']['diretoria'];
                $area = "Diretoria";
                break;
            case '1' : $destinatario = $webpecas['email']['comercial'];
                $area = "Comercial";
                break;
            case '2' : $destinatario = $webpecas['email']['financeiro'];
                $area = "Financeiro";
                break;
            case '3' : $destinatario = $webpecas['email']['suporte'];
                $area = "Suporte";
                break;
            case '4' : $destinatario = $webpecas['email']['rh'];
                $area = " Recursos Humanos";
                break;
            case '5' : $destinatario = $webpecas['email']['marketing'];
                $area = "Marketing";
                break;
            case '6' : $destinatario = $webpecas['email']['ti'];
                $area = "Tecnologia";
                break;
        }

        $email_remetente = $nome . '<webpecas@webpecas.com.br>'; // deve ser um email do dominio
        //====================================================
        //Configurações do email, ajustar conforme necessidade
        //====================================================
        $email_destinatario = $destinatario; // qualquer email pode receber os dados
        $email_reply = $nome . "<" . $email . ">";
        $email_assunto = "Contato com " . $area . " via Site WEBPECAS";
        //====================================================
        //Monta o Corpo da Mensagem
        //====================================================
        $email_conteudo = '
Nome :' . $nome . '
Email : ' . $email . '
<hr></hr>
Comentários :' . $mensagem . '

';
        //====================================================
        //Seta os Headers (Alerar somente caso necessario)
        //====================================================
        $email_headers = implode("\n", array("From: $email_remetente", "Reply-To: $email_reply", "Subject: $email_assunto", "Return-Path:  $email_remetente", "MIME-Version: 1.0", "X-Priority: 3", "Content-Type: text/html; charset=UTF-8"));
        //====================================================
        //Enviando o email
        //====================================================
        if (mail($email_destinatario, $email_assunto, nl2br($email_conteudo), $email_headers)) {
            if ($webpecas['sms']['email_contato'] == true) {
                // teste de envio//
                $envio = array(
                    'assunto' => 'Recebendo email via site', // Assunto
                    'mensagem' => 'Contato : Depto ' . $area . ' Mensagem : ' . $mensagem,
                    'numeros' => '11991421677' // numero de recebimento  
                );
                EnviaSms($envio);
            }
            echo "</b><h1><p class=\"bg-success\">E-Mail enviado com sucesso!</p></h1></b>";
        } else {
            echo "</b><p class=\"bg-warning\">Falha no envio do E-Mail!</b></p>";
        }
        //====================================================
    }
    IF(isset($_SESSION['usuario'])){
        $nome = $_SESSION['usuario']['nome'];
        $sobreNome = $_SESSION['usuario']['sobreNome'];
        $email = $_SESSION['usuario']['email'];
    }else{
        $nome = "";
        $sobreNome = "";
        $email = "";
    }
    ?>


    <h2> Contato</h2>
    <form action="<?php $PHP_SELF; ?>" method="POST"><br>
        Departamento:<br>
        <select name="destinatario">
            <option value="0">Diretoria</option>
            <option value="1">Comercial</option>
            <option value="2">Financeiro</option>
            <option value="3" selected="selected">Suporte</option>
            <option value="4">Recursos Humanos</option>
            <option value="5">Marketing</option>
            <option value="6">Tecnologia da Informática</option>
        </select><br>

        <input type="hidden" name="subject" value="Contato via site Webpeças.com.br">

        Nome:<br><input type="text" name="nome" value="<?php echo $nome . " " . $sobreNome ; ?>"size="40" ><br>
        e-mail:<br> <input type="text" name="email" value="<?php echo $email ; ?>" size="40" ><br>
        Comentários:<br>
        <textarea name="mensagem" rows="5" cols="60" wrap="off"></textarea><br><br />
        <input type="submit" name="BTEnvia" value="Enviar">
        <input type="reset" name="BTApaga" value="Apagar">
        <br />
        <br />
    </form>
</div>       
</div>



<?php require"layoutRodape.php"; ?>
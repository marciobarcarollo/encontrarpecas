<?php require"layoutTopo.php"; ?>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>Disparamos seu Orçamento de Peças</h1>
        <p>Preencha o formulário de orçamento , e receba cotações de várias empresas de auto peças .</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
    </div>
</div>

<!-- BLOCOS HEADLIGTH -->
<div class="container-fluid">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-3">
            <h3 class="text-center"><img src="http://localhost/New_Webpecas/inc/glyphicons_free/glyphicons/png/glyphicons-228-usd.png"> Sem Custo</h3>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-3">
            <h3 class="text-center"><img src="http://localhost/New_Webpecas/inc/glyphicons_free/glyphicons/png/glyphicons-10-magic.png"> Agil</h3>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-3">
            <h3 class="text-center"><img src="http://localhost/New_Webpecas/inc/glyphicons_free/glyphicons/png/glyphicons-281-settings.png"> Prático</h3>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-3">
            <h3 class="text-center"><img src="http://localhost/New_Webpecas/inc/glyphicons_free/glyphicons/png/glyphicons-29-cars.png"> Economise</h3>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <hr>    
    </div>
    <div class="row">
        <div class="col-md-2"><p class="text-center"><h2 class="text-center">2645</h2></p><h6 class="text-center">Cotações<br />Publicadas</h6></a></div>
        <div class="col-md-2"><p class="text-center"><h2 class="text-center"><?php echo QtdUsuarios(); ?></h2></p><h6 class="text-center">Comunidade<br />Usuários</h6></div>
        <div class="col-md-2"><p class="text-center"><h2 class="text-center">67</h2></p><h6 class="text-center">Comunidade<br />Assinantes</h6></div>
        <div class="col-md-2"><p class="text-center"><h2 class="text-center">36</h2></p><h6 class="text-center">Estoques<br />Publicados</h6></div>
        <div class="col-md-2"><p class="text-center"><h2 class="text-center">2345600</h2></p><h6 class="text-center">Produtos<br />Cadastrados</h6></div> 
        <div class="col-md-2"><p class="text-center"><h2 class="text-center">67</h2></p><h6 class="text-center">Marcas<br />Cadastradas</h6></div>
    </div> 
    <div class="row">
        <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
        <div class="col-xs-12 col-sm-6 col-md-8">.col-xs-12 .col-sm-6 .col-md-8</div>    
    </div>
</div><!-- /container-fluid -->
<!-- Dados de Cadastro -->
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-2 col-md-2"></div>
        <div class="col-sm-6 col-md-8"><?php require "./inc/google/maps/getLatLong.php"; ?></div>
        <div class="col-xs-2 col-md-2"></div>
    </div> 
    <hr>
</div><!-- /container-fluid -->

<?php require"layoutRodape.php"; ?>
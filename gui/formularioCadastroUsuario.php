<?php
$id = "";
$nome = "";
$sobreNome = "";
$verificado = "";
$email = "";
$emailConfirmacao = "";
$senha = "";
$senhaConfirmacao = "";
$dataCadastro = "";
$status = "";
$erro = "";
$sucesso = "";

$usuario = $this->getDados("usuario");

if ($usuario) {

    $usuario instanceof NovoUsuario;

    $id = $usuario->getId();
    $nome = $usuario->getNome();
    $sobreNome = $usuario->getSobreNome();
    $email = $usuario->getEmail();
    $emailConfirmacao = $usuario->getEmailConfirmacao();
    $senha = $usuario->getSenha();
    $senhaConfirmacao = $usuario->getSenhaConfirmacao();
    $status = $usuario->getStatus();
    $erro = $usuario->getErro();
    $sucesso = $usuario->getSucesso();
}
// id, nome, sobreNome, verificado, senha, dataCadastro, status
?>
<?php require"layoutTopo.php"; ?>

        <!--required required autofocus -->
        <div class="container-fluid">
            <div class="row">
                <pre>
            <form method="POST" action="<?php echo URL; ?>controle-usuario/salvar">
                        <?php
                        IF ($erro) {
                            echo "<div class=\"alert alert-danger\" role=\"alert\">";
                            foreach ($erro as $value) {
                                echo "<p><span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>";
                                echo "<span class=\"sr-only\">Error:</span>";
                                echo $value . "</p>";
                            }
                            echo "</div>";
                        } else if ($sucesso) {
                            //echo "<div class=\"alert alert-sucsses\" role=\"alert\">";
                            foreach ($sucesso as $value) {
                                echo "<p class=\"bg-success\"><i class=\"glyphicon glyphicon-user\"></i> - {$value} </p> <p class=\"bg-info\"><i class=\"glyphicon glyphicon-info-sign\"></i> - Enviamos um email para a Verificação e Ativação de Cadastro.</p>";
                            }
                            //echo "</div>";
                        }
                        ?>          
                <label><h1>Cadastro de Usuário</h1></label>
                <label>Nome</label>
                <input type="text" name="nome" value="<?php echo $nome; ?>" required autofocus >
                <label>Sobre nome</label>
                <input type="text"  name="sobreNome" value="<?php echo $sobreNome; ?>" required>
                <label>Email</label>
                <input type="email"  name="email" value="<?php echo $email; ?>" required>
                <label>Confirmacao de Email</label>
                <input type="email"  name="emailConfirmacao" value="<?php echo $emailConfirmacao; ?>" required >
                <label>Senha</label>
                <input type="password" name="senha" value="<?php echo $senha; ?>" required >
                <label>Confirmacao de senha</label>
                <input type="password" name="senhaConfirmacao" value="<?php echo $senhaConfirmacao; ?>" required >
                <button class="btn btn-lg btn-success" type="submit">Criar conta</button>
                
                <input type="hidden" readonly="true" value="" name="id">
                <input type="hidden" readonly="true" name="verificado" value="0" >
                <input type="hidden" name="dataCadastro" value="<?php echo $dataCadastro; ?>" >
                <input type="hidden" readonly="true" value="<?php echo $id; ?>" name="id">
                <input type="hidden" name="status" value="0" >                
            </form>
                </pre>
            </div>
        </div> <!-- /container -->

        <footer>
            <p>Webpecas Serviços em AutoPeças Ltda - 2005</p>
        </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo URL; ?>inc/bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo URL; ?>inc/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>

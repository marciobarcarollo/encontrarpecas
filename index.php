<?php

session_start();
header('Content-Type: text/html; charset=utf-8');
setlocale(LC_ALL, 'ptb', 'portuguese-brazil', 'pt-br', 'bra', 'brazil');
date_default_timezone_set('America/Sao_Paulo');
// iniset('display_errors',1);
// ini_set('log_errors',1);
// ini_set('error_log',  dirname(__FILE__).'error_log.txt');
// error_reporting(E_ALL);

require_once './config/config.php';

/**
 * Método de Carregamento automático de classes
 * @param class $c classe que será instanciada.
 */
function __autoload($c) {
    $diretorios = array(
        './',
        './to/',
        './model/',
        './libs/',
        './gui/',
        './dao/',
        './config/',
        './inc/google/maps/'
    );
    foreach ($diretorios as $dir) {
        if (file_exists($dir . $c . ".php")) {
            require_once $dir . $c . ".php";
        }
    }
}

/**
 *  Estabelecemos a Geolocalização
 */
if (!isset($_SESSION['geoip'])) {
    $ip = $_SERVER['REMOTE_ADDR'];
    $g = new Geoip();
    $g->setIp($ip);

    $dg = new DaoGeoip();
    $retg = $dg->listar($g->getIp());

    if ($retg instanceof Geoip) {
        // Mandamos para sessão de usuário
        $_SESSION['geoip']['ip'] = $retg->getIp();
        $_SESSION['geoip']['countryCode'] = $retg->getCountryCode();
        $_SESSION['geoip']['regionCode'] = $retg->getRegionCode();
        $_SESSION['geoip']['cityName'] = $retg->getCityName();
        $_SESSION['geoip']['postalCode'] = $retg->getPostalCode();
        $_SESSION['geoip']['latitude'] = $retg->getLatitude();
        $_SESSION['geoip']['longitude'] = $retg->getLongitude();
        $_SESSION['geoip']['metroCode'] = $retg->getMetroCode();
        $_SESSION['geoip']['areaCode'] = $retg->getAreaCode();
        $_SESSION['geoip']['ispName'] = $retg->getIspName();
        $_SESSION['geoip']['organizationName'] = $retg->getOrganizationName();
        $_SESSION['geoip']['error'] = $retg->getError();
        $_SESSION['geoip']['data'] = $retg->getData();
        $_SESSION['geoip']['status'] = $retg->getStatus();
    } else {
        IF ($webpecas['maxmind']['status'] != FALSE) {
            //REQUISITAMOS AO SERVIÇO MAXMIND
            $maxmind = MaxmindGeoIp($g->getIp());

            $g->setCountryCode($maxmind[0]);
            $g->setRegionCode($maxmind[1]);
            $g->setCityName(utf8_encode($maxmind[2]));
            $g->setPostalCode($maxmind[3]);
            $g->setLatitude($maxmind[4]);
            $g->setLongitude($maxmind[5]);
            $g->setMetroCode($maxmind[6]);
            $g->setAreaCode($maxmind[7]);
            $g->setIspName($maxmind[8]);
            $g->setOrganizationName($maxmind[9]);
            //$g->setError($maxmind[10]);
            
        }
        
        $g->setStatus('1');
        $dataCadastro = date("Y-m-d H:i:s");
        $g->setData($dataCadastro);

        //SALVAMOS NO BANCO DE DADOS OS DADOS DE GEOIP
        $salvar_dg = $dg->salvar($g);
        if ($salvar_dg instanceof Geoip) {

            // Mandamos para sessão de usuário
            $_SESSION['geoip']['ip'] = $g->getIp();
            $_SESSION['geoip']['countryCode'] = $g->getCountryCode();
            $_SESSION['geoip']['regionCode'] = $g->getRegionCode();
            $_SESSION['geoip']['cityName'] = $g->getCityName();
            $_SESSION['geoip']['postalCode'] = $g->getPostalCode();
            $_SESSION['geoip']['latitude'] = $g->getLatitude();
            $_SESSION['geoip']['longitude'] = $g->getLongitude();
            $_SESSION['geoip']['metroCode'] = $g->getMetroCode();
            $_SESSION['geoip']['areaCode'] = $g->getAreaCode();
            $_SESSION['geoip']['ispName'] = $g->getIspName();
            $_SESSION['geoip']['organizationName'] = $g->getOrganizationName();
            $_SESSION['geoip']['error'] = $g->getError();
            $_SESSION['geoip']['data'] = $g->getData();
            $_SESSION['geoip']['status'] = $g->getStatus();
        } else {
            //tratar erro
        }
    }
}

/**
 * Estabelecemos uma Sessão.
 */
if (!isset($_SESSION['session'])) {

    $idSession = "";
    $ip = $_SERVER['REMOTE_ADDR'];
    $dataSession = date("Y-m-d H:i:s");
    $tokem = session_id();
    if (isset($_SERVER['HTTP_REFERER'])) {
        $httpHeferer = $_SERVER['HTTP_REFERER'];
    }


    $s = new Session();

    $s->setIdSession($idSession);
    $s->setIp($ip);
    $s->setDataSession($dataSession);
    $s->setTokemSession($tokem);
    $s->setHttpReferer($httpHeferer);

    $ds = new DaoSession();
    $salvar_ds = $ds->salvar($s);

    if ($salvar_ds instanceof Session) {
        // MANDAMOS PARA SESSÂO DE USUARIO
        $_SESSION['session']['id'] = $salvar_ds->getIdSession();
        $_SESSION['session']['tokem'] = $salvar_ds->getTokemSession();
    } else {
        // tratar erro de salvar session
    }
}

$app = new TApp();
$app->executar();
?>
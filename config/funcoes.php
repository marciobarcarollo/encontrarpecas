<?php

/*
 * Funções utilizadas dentro do sistema
 */

/**
 * RETORNA O ARRAY DE SESSION PARA O IP
 */
function MaxmindGeoIp($ip) {
    GLOBAL $webpecas;
    $buf = "";
    $query = "https://geoip.maxmind.com/f?l=" . $webpecas['maxmind']['licence_key'] . "&i=" . $ip;
    $url = parse_url($query);
    $host = $url["host"];
    $path = $url["path"] . "?" . $url["query"];
    $timeout = 1;
    $fp = fsockopen($host, 80, $errno, $errstr, $timeout)
            or die('Can not open connection to server.');
    if ($fp) {
        fputs($fp, "GET $path HTTP/1.0\nHost: " . $host . "\n\n");
        while (!feof($fp)) {
            $buf .= fgets($fp, 128);
        }
        $lines = explode("\n", $buf);
        $data = $lines[count($lines) - 1];
        fclose($fp);
    } else {
        # enter error handing code here
    }
    $linha = explode(',', $data);
    return $linha;
}

function QtdUsuarios() {
    $du = new DaoUsuario();
    $qtdUsuarios = $du->contaUsuarios();
    return $qtdUsuarios;
}

function EnviaSms($sms){
    GLOBAL $webpecas;
    $webservice_url = $webpecas['sms']['url'];
    $webservice_query    = array(
        'lgn' => $webpecas['sms']['login'], // LOGIN    
        'pwd' => $webpecas['sms']['senha'], // SENHA
        'subject' => $sms['assunto'], // Assunto
        'msg' => $sms['mensagem'],
        'numbers' => $sms['numeros'] //11991421677' // numeros de recebimento    
    );

    //Forma URL
    $webservice_url .= '?action=sendsms&';
    foreach($webservice_query as $get_key => $get_value){
        $webservice_url .= $get_key.'='.urlencode($get_value).'&';
    }

    parse_str(file_get_contents($webservice_url), $resultado );

    //print_r($resultado);
    return; 
}

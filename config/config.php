<?php
// BANCO DE DADOS 

$banco = 'producao';

IF ($banco != 'producao') {
// Banco de Dados LOCALHOST.
    define("MYDB_HOST", "127.0.0.1");
    define("MYDB_PORT", "3306");
    define("MYDB_NAME", "webpecas_03");
    define("MYDB_USER_NAME", "root");
    define("MYDB_PASSWORD", "");

    define("URL", "http://www.encontrarpecas.com.br/");
} else {
    // Banco de Dados ENCONTRARPECAS.
    define("MYDB_HOST", "198.154.255.55");
    define("MYDB_PORT", "3306");
    define("MYDB_NAME", "encontra_01");
    define("MYDB_USER_NAME", "encontra_01");
    define("MYDB_PASSWORD", "f5d3f5d3");

    define("URL", "http://www.encontrarpecas.com.br/");
}

// DIRETORIO INC
define("URL_BOOTSTRAP", "inc/bootstrap/"); // http://www.getbootstrap.com/
define("URL_SMARTY","inc/smarty/"); // http://www.smarty.net



/**
 * Parametros de GLOBAIS DO SISTEMA.
 */
$GLOBALS['webpecas'] = array(
// MAXMIND // To access the products you purchased, go to http://www.maxmind.com/en/account
    'maxmind' => array(
        'status' => 'FALSE', // FALSE = DESLIGA o Maxmind , TRUE = LIGA O MAXMIND 
        'id' => '68385',
        'licence_key' => 'UmGYQaTTSCfH',
        'username' => 'financeiro@webpecas.com.br',
        'password' => 'f5d3f5d3',
        'url' => 'https://www.maxmind.com/en/account'
    ),
    'remetente' => array(
        'cadastro' => 'From: Webpeças Cadastro <no_replay@webpecas.com.br>' . "\r\n"
    ),
    // EMAILS
    'email'=> array( 
        'suporte' => 'suporte@webpecas.com.br',
        'financeiro' => 'financeiro@webpecas.com.br',
        'webpecas' => 'webpecas@webpecas.com.br',
        'cadastro' => 'cadastro@webpecas.com.br',
        'cotacoes' => 'cotacoes@webpecas.com.br',
        'diretoria' => 'suporte@webpecas.com.br',
        'comercial' => 'suporte@webpecas.com.br',
        'rh' =>'suporte@webpecas.com.br',
        'marketing' =>'suporte@webpecas.com.br',
        'ti' =>'suporte@webpecas.com.br'
        ),
    // SKYPE
    'skype'=> array(
        'id' => 'webpecas.contato'
        ),
    // SMS //www.smsgenial.com.br
    'sms'=> array(
        'url' => 'https://smsgenial.com.br/sms/app/modulo/api/', // url de destino,
        'login' => '11991298778', //'11991421677'
        'senha' => 'f5d3f5d3', // 'f5d3f5d3'
        'novo_publicador' => true, // QUANDO OCORRE UM CADASTRO DE NOVO ASSINANTE
        'nova_cotacao' => false, // SMS AVISANDO DE NOVA COTAÇÃO NO SISTEMA.
        'desativacao' => false, // AVISA AOS USUARIOS DO ASSINANTE QUE FOI DESATIVADO.
        'quitacao_mensalidade' => false, // QUANDO OCORRE A QUITAÇÃO DE MENSALIDADE.
        'update_estoque' => true, // OCORRE QUANDO REALIZADO UM UPDATE DE ESTOQUE.
        'email_contato' => true, // solicitação de email de contato através do site.
        'email_suporte' => false // solicitação de email de usuário logado.
        )
);

/**
 * REQUISIÇÂO DAS FUNÇOES COMUNS DO SISTEMA
 */
global $webpecas;

require_once 'funcoes.php';

?>
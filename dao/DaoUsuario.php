<?php

/**
 * Description of DaoUsuario
 *
 * @author marcio barcarrollo
 */
class DaoUsuario {

    public function excluir(Usuario $u) {
        $sql = "DELETE FROM tb_usuario Where id =:ID";

        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        $p1 = $u->getId();
        $sth->bindParam("ID", $p1);
        try {
            $sth->execute();
            return TRUE;
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }

    /**
     * 
     * @param int $p1
     * @return Usuario
     */
    public function listar($p1) {
        // verificação de $P1

        $sql = "SELECT id, nome, sobre_Nome, verificado, senha, data_Cadastro, status FROM tb_usuario Where id=:ID";

        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        $sth->bindParam("ID", $p1);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        $usu = $sth->fetchObject("Usuario");
        return $usu;
    }

    public function listarTodos() {
        $sql = "SELECT id, nome, sobre_Nome, verificado, senha, data_Cadastro, status FROM tb_usuario";

        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        $arUsu = array();
        while ($usu = $sth->fetchObject("Usuario")) {
            $arUsu[] = $usu;
        }
        return $arUsu;
    }

    public function salvar(Usuario $u) {

        $nome = $u->getNome();
        $sobreNome = $u->getSobreNome();
        $verificado = $u->getVerificado();
        $senha = $u->getSenha();
        $dataCadastro = $u->getDataCadastro();
        $status = $u->getStatus();

        if ($u->getId()) {
            $id = $u->getId();
            $sql = "UPDATE tb_usuario set nome=:nome , login=:login , senha=:senha , status=:status WHERE id = :id ";
        } else {
            $id = $this->generateID();
            $u->setId($id);
            $sql = "INSERT INTO tb_usuario (id, nome, sobreNome, verificado, senha, dataCadastro, status) values (:id , :nome , :sobreNome , :verificado , :senha , :dataCadastro , :status)";
        }
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);


        $sth->bindParam("id", $id);
        $sth->bindParam("nome", $nome);
        $sth->bindParam("sobreNome", $sobreNome);
        $sth->bindParam("verificado", $verificado);
        $sth->bindParam("senha", $senha);
        $sth->bindParam("dataCadastro", $dataCadastro);
        $sth->bindParam("status", $status);

        try {
            $sth->execute();
            return $u;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * 
     * @return int
     */
    private function generateID() {
        $sql = "SELECT (coalesce(max(id),0)+1) as id FROM tb_usuario ";
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
        $res = $sth->fetch();
        $id = $res['id'];
        return $id;
    }

    public function autenticar($login, $senha) {
        $sql = "SELECT ehu.usuarioId as id, us.nome, us.sobreNome, us.verificado as  usuarioVerificado, us.dataCadastro, us.status, em.email, em.verificado as emailVerificado, em.principal
                FROM tb_usuario_has_tb_email as ehu
                Inner join tb_usuario as us on us.id = ehu.usuarioId
                Inner join tb_email as em on em.id = ehu.emailId
                Where em.email = :LOGIN
                And us.senha = :SENHA 
                ";

        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        $sth->bindParam("LOGIN", $login);
        $sth->bindParam("SENHA", $senha);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
//        $res = $sth->fetch();
//        $id = $res['U'];
//        return $id > 0;
        $usu = $sth->fetch(PDO::FETCH_ASSOC);
        return $usu;
    }

    public function verificar(Usuario $u) {

        $id = $u->getId();
        $verificado = $u->getVerificado();
        $status = $u->getStatus();

        $sql = "UPDATE tb_usuario set verificado = :verificado , status = :status WHERE id = :id ";

        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);

        $sth->bindParam("id", $id);
        $sth->bindParam("verificado", $verificado);
        $sth->bindParam("status", $status);

        try {
            $sth->execute();
            return $u;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function contaUsuarios() {
        $sql = " SELECT count(id) as quantidade FROM tb_usuario WHERE verificado = '1' ";
        
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
        $res = $sth->fetch();
        $qtd = $res['quantidade'];
        return $qtd;
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DaoEmail
 *
 * @author marcio barcarrollo
 */
class DaoEmail {

    public function excluir(Email $e) {
        
    }

    public function listar($p1) {

        $sql = "SELECT id, email, verificado, principal FROM tb_email Where email =:EMAIL or id =:EMAIL";

        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        $sth->bindParam("EMAIL", $p1);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        $em = $sth->fetchObject("Email");
        return $em;
    }

    public function listarTodos() {
        
    }

    public function salvar(Email $e) {

        $id = $e->getId();
        $email = $e->getEmail();
        $verificado = $e->getVerificado();
        $principal = $e->getPrincipal();

        if ($e->getId()) {
            $id = $e->getId();
            $sql = "UPDATE tb_usuario set nome=:nome , login=:login , senha=:senha , status=:status WHERE id = :id ";
        } else {
            $id = $this->generateID();
            $e->setId($id);
            $sql = "INSERT INTO tb_email (id, email, verificado, principal) values (:id , :email , :verificado , :principal )";
        }
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);


        $sth->bindParam("id", $id);
        $sth->bindParam("email", $email);
        $sth->bindParam("verificado", $verificado);
        $sth->bindParam("principal", $principal);

        try {
            $sth->execute();
            return $e;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * 
     * @return int
     */
    private function generateID() {
        $sql = "SELECT (coalesce(max(id),0)+1) as id FROM tb_email ";
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
        $res = $sth->fetch();
        $id = $res['id'];
        return $id;
    }

    public function verificar(Email $e) {
        
        $id = $e->getId();
        $verificado = $e->getVerificado();
      
        $sql = "UPDATE tb_email set verificado = :verificado WHERE id = :id ";

        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);

        $sth->bindParam("id", $id);
        $sth->bindParam("verificado", $verificado);

        try {
            $sth->execute();
            return $e;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DaoEmailHasUsuario
 *
 * @author marcio barcarrollo
 */
class DaoEmailHasUsuario {

    public function excluir(EmailHasUsuario $ehu) {
        
    }

    public function listar($p1) {
        $p1 = isset($p1) ? $p1 : FALSE;
        if (!$p1) {
            return;
        } else {
            $usuarioId = $p1['usuarioId'];
            $emailId = $p1['emailId'];

            $sql = "SELECT usuarioId, emailId FROM tb_usuario_has_tb_email Where usuarioId =:usuarioId AND emailId = :emailId";

            $conexao = Conexao::getConexao();
            $sth = $conexao->prepare($sql);
            $sth->bindParam("usuarioId", $usuarioId);
            $sth->bindParam("emailId", $emailId);

            try {
                $sth->execute();
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
            $ehu = $sth->fetch(PDO::FETCH_ASSOC);
            return $ehu;
        }
    }

    public function listarTodos() {
        
    }

    public function salvar(EmailHasUsuario $ehu) {

        $emailId = $ehu->getEmailId();
        $usuarioId = $ehu->getUsuarioId();

        if (!$emailId || !$usuarioId) {
            echo " realacionamento não pode ser cadastrado";
        } else {
            $sql = "INSERT INTO tb_usuario_has_tb_email (usuarioId, emailId) values (:usuarioId , :emailId )";
        }
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        $sth->bindParam("emailId", $emailId);
        $sth->bindParam("usuarioId", $usuarioId);

        try {
            $sth->execute();
            return $ehu;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}

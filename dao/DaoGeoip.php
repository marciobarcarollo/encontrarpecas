<?php

/**
 * Operações com o Banco de dados tb_geoip
 *
 * @author marcio barcarrollo
 */
class DaoGeoip extends Geoip{
    
    public function excluir(Geoip $g) {
        
    }

    public function listar($p1) {
        $sql = "SELECT * FROM tb_geoip WHERE ip = :IP";

        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        $sth->bindParam("IP", $p1);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        $g = $sth->fetchObject("Geoip");
        return $g;
    }

    public function listarTodos() {
        
    }

    public function salvar(Geoip $g) {
     
        $sql = "INSERT INTO tb_geoip (ip, countryCode, regionCode, cityName, postalCode, latitude, longitude, metroCode, areaCode, ispName, organizationName, error, data, status) values (:ip, :countryCode, :regionCode, :cityName, :postalCode, :latitude, :longitude, :metroCode, :areaCode, :ispName, :organizationName, :error, :data, :status)";

        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);

        $sth->bindParam("ip", $g->getIp());
        $sth->bindParam("countryCode", $g->getCountryCode());
        $sth->bindParam("regionCode", $g->getCityName());
        $sth->bindParam("cityName", $g->getCityName());
        $sth->bindParam("postalCode", $g->getPostalCode());
        $sth->bindParam("latitude", $g->getLatitude());
        $sth->bindParam("longitude", $g->getLongitude());
        $sth->bindParam("metroCode", $g->getMetroCode());
        $sth->bindParam("areaCode", $g->getAreaCode());
        $sth->bindParam("ispName", $g->getIspName());
        $sth->bindParam("organizationName", $g->getOrganizationName());
        $sth->bindParam("error", $g->getError());
        $sth->bindParam("data", $g->getData());
        $sth->bindParam("status", $g->getStatus());

        try {
            $sth->execute();
            return $g;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}

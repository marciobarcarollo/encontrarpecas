<?php

/**
 * Operações com o Banco de dados tb_session
 *
 * @author marcio barcarrollo
 */
class DaoSession {

    public function excluir(Session $s) {
        
    }

    public function listar($p1) {
        
    }

    public function listarTodos() {
        
    }

    public function salvar(Session $s) {

        $idSession = $s->getIdSession();
        $dataSession = $s->getDataSession();
        $tokemSession = $s->getTokemSession();
        $ipSession = $s->getIp();
        $httpReferer = $s->getHttpReferer();

        if ($s->getIdSession()) {
            $idSession = $s->getIdSession();
            $sql = "UPDATE tb_usuario set nome=:nome , login=:login , senha=:senha , status=:status WHERE id = :id ";
        } else {
            $idSession = $this->generateID();
            $s->setIdSession($idSession);
            $sql = "INSERT INTO tb_session (idSession, tokemSession, httpReferer, dataSession, ip) values (:idSession, :tokemSession, :httpReferer, :dataSession, :ip)";
        }
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);

        //idSession, tokemSession, httpReferer, dataSession, ip
        $sth->bindParam("idSession", $idSession);
        $sth->bindParam("dataSession", $dataSession);
        $sth->bindParam("tokemSession", $tokemSession);
        $sth->bindParam("ip", $ipSession);
        $sth->bindParam("httpReferer", $httpReferer);

        try {
            $sth->execute();
            return $s;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * 
     * @return int
     */
    private function generateID() {
        $sql = "SELECT (coalesce(max(idSession),0)+1) as id FROM tb_session ";
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
        $res = $sth->fetch();
        $id = $res['id'];
        return $id;
    }

}

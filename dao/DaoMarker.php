<?php

/**
 * Description of DaoMarker
 *
 * @author marcio barcarrollo
 */
class DaoMarker extends Marker {

    public function listar() {
        // Select all the rows in the markers table
        $sql = "SELECT * FROM tb_markers WHERE 1 ";
        
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
        $markers = array();
        while ($marker = $sth->fetchObject("Marker")) {
            $markers[] = $marker;
        }
        return $markers;
    }

}

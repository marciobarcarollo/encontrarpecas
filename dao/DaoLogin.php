<?php

/**
 * Operações com o Banco de dados tb_login
 *
 * @author marcio barcarrollo
 */
class DaoLogin {

    public function excluir(ModLogin $l) {
        
    }

    public function listar($p1) {
        
    }

    public function listarTodos() {
        
    }

    public function salvar(ModLogin $l) {
                
        $id = $l->getId();
        $data = $l->getData();
        $idSession = $l->getIdSession();
        $idUsuario = $l->getIdUsuario();

            $id = $this->generateID();
            $l->setId($id);
            $sql = "INSERT INTO tb_login (id, data, idSession, idUsuario) values ( :id , :data , :idSession , :idUsuario)";
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);

        //id, data, idSession, idUsuario
        $sth->bindParam("id", $id);
        $sth->bindParam("data", $data);
        $sth->bindParam("idSession", $idSession);
        $sth->bindParam("idUsuario", $idUsuario);

        try {
            $sth->execute();
            return $l;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    /**
     * 
     * @return int
     */
    private function generateID() {
        $sql = "SELECT (coalesce(max(id),0)+1) as id FROM tb_login ";
        $conexao = Conexao::getConexao();
        $sth = $conexao->prepare($sql);
        try {
            $sth->execute();
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
        $res = $sth->fetch();
        $id = $res['id'];
        return $id;
    }

}
